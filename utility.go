package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"gopkg.in/session.v1"
)

var (
	config = oauth2.Config{
		ClientID:     "222222",
		ClientSecret: "22222222",
		Scopes:       []string{"all"},
		RedirectURL:  "https://pitangui.amazon.com/api/skill/link/M256OAZNG882Y2",
		Endpoint: oauth2.Endpoint{
			AuthURL:  "/authorize",
			TokenURL: "/token",
		},
	}
	globalSessions *session.Manager
)

func init() {

	globalSessions, _ = session.NewManager("memory", `{"cookieName":"gosessionid","gclifetime":3600}`)
	go globalSessions.GC()

}
func LoginHandler(w http.ResponseWriter, r *http.Request) {
	var l_userId string
	log.Println("loginHandler")
	log.Println("request url is", r.RequestURI)
	log.Println("request method", r.Method)
	//requestbody, _ := ioutil.ReadAll(r.Body)
	//	log.Println("request body is", string(requestbody))
	if r.Method == "POST" {
		db, err := sql.Open("mysql", "b05b8e6c9907b4:71474186@tcp(us-cdbr-azure-west-b.cleardb.com:3306)/alexa_dev_db")
		if err != nil {
			panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
		}
		// Open doesn't open a connection. Validate DSN data:
		err = db.Ping()
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
		r.ParseForm()
		// logic part of log in
		r_username := strings.Join(r.Form["username"], "")
		r_password := strings.Join(r.Form["password"], "")
		fmt.Println("username is", r_username)
		fmt.Println("password is", r_password)
		err = db.QueryRow("SELECT id FROM `user_login` where username=? and password=?", r_username, r_password).Scan(&l_userId)
		if strings.EqualFold(l_userId, "") {
			fmt.Println("username does not exist")
			outputHTML(w, r, "static/login.html")
		}
		fmt.Println("before session")
		us, err := globalSessions.SessionStart(w, r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		fmt.Println("after session")

		fmt.Println("before userid")
		us.Set("LoggedInUserID", l_userId)
		fmt.Println("after userid")
		w.Header().Set("Location", "/auth")
		w.WriteHeader(http.StatusFound)
		defer db.Close()
		return
	}
	outputHTML(w, r, "static/login.html")
}

func outputHTML(w http.ResponseWriter, req *http.Request, filename string) {
	log.Println("outputHTML")
	requestbody, _ := ioutil.ReadAll(req.Body)
	log.Println("request body is", string(requestbody))
	log.Println("request body is", requestbody)
	file, err := os.Open(filename)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	defer file.Close()
	fi, _ := file.Stat()
	http.ServeContent(w, req, file.Name(), fi.ModTime(), file)
}

func Handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Awesome! - Go go go!")
}

func ClientHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("client")
	u := config.AuthCodeURL("xyz")
	http.Redirect(w, r, u, http.StatusFound)
}

func Oauth2Handler(w http.ResponseWriter, r *http.Request) {
	log.Println("oauth2")

	r.ParseForm()
	state := r.Form.Get("state")
	if state != "xyz" {
		http.Error(w, "State invalid", http.StatusBadRequest)
		return
	}
	code := r.Form.Get("code")
	log.Println("code is", code)
	if code == "" {
		http.Error(w, "Code not found", http.StatusBadRequest)
		return
	}
	token, err := config.Exchange(context.Background(), code)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Println("w is:", *token)
	e := json.NewEncoder(w)
	e.SetIndent("", "  ")
	e.Encode(*token)
}

func AuthHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("authHandler")

	us, err := globalSessions.SessionStart(w, r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Println("LoggedInUserID:", us.Get("LoggedInUserID"))
	if us.Get("LoggedInUserID") == nil {
		w.Header().Set("Location", "/login")
		w.WriteHeader(http.StatusFound)
		return
	}
	if r.Method == "POST" {
		form := us.Get("Form").(url.Values)
		log.Println("form values entered are", form)
		u := new(url.URL)
		u.Path = "/authorize"
		u.RawQuery = form.Encode()
		w.Header().Set("Location", u.String())
		w.WriteHeader(http.StatusFound)
		us.Delete("Form")
		us.Set("UserID", us.Get("LoggedInUserID"))
		return
	}
	outputHTML(w, r, "static/auth.html")
}
func UserAuthorizeHandler(w http.ResponseWriter, r *http.Request) (userID string, err error) {
	log.Println("userAuthorizeHandler")
	us, err := globalSessions.SessionStart(w, r)
	uid := us.Get("UserID")
	if uid == nil {
		if r.Form == nil {
			r.ParseForm()
		}
		us.Set("Form", r.Form)
		w.Header().Set("Location", "/login")
		w.WriteHeader(http.StatusFound)
		return
	}
	userID = uid.(string)
	us.Delete("UserID")
	return
}
