package main

//added code to save Access token
//added internal repo

import (
	"log"
	"net/http"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/session.v1"

	"bitbucket.org/saltmines/alexa-login/server"
	_ "golang.org/x/oauth2"
	"gopkg.in/oauth2.v3/errors"
	"gopkg.in/oauth2.v3/manage"
	"gopkg.in/oauth2.v3/models"
	"gopkg.in/oauth2.v3/store"
)

func init() {

	globalSessions, _ = session.NewManager("memory", `{"cookieName":"gosessionid","gclifetime":3600}`)
	go globalSessions.GC()

}

func main() {
	http.HandleFunc("/", Handler)

	//------------------------------client.go--------------------------------------------//
	http.HandleFunc("/client", ClientHandler)
	http.HandleFunc("/oauth2", Oauth2Handler)
	//------------------------------client.go--------------------------------------------//

	//------------------------------server.go--------------------------------------------//
	manager := manage.NewDefaultManager()
	// token store
	manager.MustTokenStorage(store.NewMemoryTokenStore())

	clientStore := store.NewClientStore()
	clientStore.Set("222222", &models.Client{
		ID:     "222222",
		Secret: "22222222",
		Domain: "",
	})
	manager.MapClientStorage(clientStore)

	srv := server.NewServer(server.NewConfig(), manager)
	srv.SetUserAuthorizationHandler(UserAuthorizeHandler)

	srv.SetInternalErrorHandler(func(err error) (re *errors.Response) {
		log.Println("Internal Error:", err.Error())
		return
	})

	srv.SetResponseErrorHandler(func(re *errors.Response) {
		log.Println("Response Error:", re.Error.Error())
	})

	http.HandleFunc("/login", LoginHandler)
	http.HandleFunc("/auth", AuthHandler)

	http.HandleFunc("/authorize", func(w http.ResponseWriter, r *http.Request) {
		log.Println("/authorize")

		err := srv.HandleAuthorizeRequest(w, r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}

	})

	http.HandleFunc("/token", func(w http.ResponseWriter, r *http.Request) {
		log.Println("/////////////token")

		//get access token here
		err := srv.HandleTokenRequest(w, r)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})
	//------------------------------server.go--------------------------------------------//

	// Azure code
	http.ListenAndServe(":"+os.Getenv("HTTP_PLATFORM_PORT"), nil)
	log.Fatal(":"+os.Getenv("HTTP_PLATFORM_PORT"), nil)
	//ROSHAN: Pushed to Azure

	// Mac code
	//http.ListenAndServe(":8080", nil)
	//log.Fatal(http.ListenAndServe(":8080", nil))
}
